let cannedAnswer;
let responseOk = true;

/**
 * @return {Object}
 */
function getCannedAnswer() {
  const data = cannedAnswer;

  setCannedAnswer();

  return data;
}

/**
 * @param {Object} [data]
 * @param {boolean} [status=true]
 */
function setCannedAnswer(data = {}, status = true) {
  cannedAnswer = data;
  responseOk = status;
}

/**
 * @return {Promise<any>}
 */
const json = () =>
  Promise
    .resolve(getCannedAnswer());

/**
 * @return {Promise<any>}
 */
const fetch = () =>
  Promise
    .resolve({
      ok: responseOk,
      json,
    });

fetch.stub = setCannedAnswer;
setCannedAnswer();

module.exports = fetch;
