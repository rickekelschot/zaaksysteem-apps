import action from '../Action';
import { login as redirect } from '../../../library/auth';
import { getUrl } from '../../../library/url';

const {
  auth: {
    login,
  },
} = action;

export default {
  [login]() {
    redirect(getUrl());

    return Promise.resolve();
  },
};
