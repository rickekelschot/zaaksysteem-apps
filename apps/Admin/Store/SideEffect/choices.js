import action from '../Action';
import { get } from '@mintlab/kitchen-sink';

const { keys } = Object;
const {
  resource: {
    respond,
  },
} = action;

/**
 * Update an item's choices after retrieving
 */
export default {
  [respond](data, store) {
    
    const NAME = 'choices';
    const STATUS_OK = 200;
    const part = keys(data).find(entry => entry === NAME);
    const payload = data[part];
    const state = store.getState();
    const { dispatch } = store;

    const status = get(payload, '$set.status');
    const name = get(payload, '$set.meta.name');
    const choices = get(payload, '$set.data');
    const items = get(state, 'resource.config.data.items');

    const index = items ? items.findIndex(item => item.name === name) : undefined;
    const shouldDispatch = () => 
      payload && status === STATUS_OK && index;

    if (shouldDispatch()) {
      dispatch(respond({
        config: { data: { items: { [index]: { choices: { $set: choices }}}}},
      }));
    }

    return Promise.resolve();
  },
};
