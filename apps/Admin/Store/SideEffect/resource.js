import action from '../Action';
import resource from '../../Resource';
import {
  callOrNothingAtAll,
  get,
} from '@mintlab/kitchen-sink';

const { assign } = Object;
const {
  auth: {
    login,
  },
  resource: {
    abort,
    request,
    respond,
  },
  system: {
    log,
  },
  ui: {
    snackbar: {
      show,
    },
  },
} = action;

function normalize(parameters) {

  const idSet = parameters
    .map(orderedPairTuple => {
      const [identifier] = orderedPairTuple;

      return (identifier.hasOwnProperty('id')) ? identifier.id : identifier;
    });

  const valuePromises = parameters
    .map(orderedPairTuple =>
      resource(...orderedPairTuple));

  return [idSet, valuePromises];
}

/**
 * A status handler is either an HTTP response code or the
 * name property of an object instance that is constructed
 * by or inherits from `Error`.
 *
 * @type {Object}
 */
const statusHandlers = {
  401() {
    return login();
  },

  403() {
    return show('server:status403');
  },

  404() {
    return log('404 NOT FOUND');
  },

  410() {
    return log('410 GONE');
  },

  500() {
    return show('server:status500');
  },

  // Low level errors:

  /**
   * The response body could not be processed with `JSON.parse`.
   *
   * @return {Object}
   */
  SyntaxError() {
    return log('SYNTAX ERROR');
  },

  /**
   * Network error or CORS misconfiguration.
   *
   * @return {Object}
   */
  TypeError() {
    return log('NETWORK OR CORS');
  },
};

/**
 * @param {Object} data
 * @return {Function|undefined}
 */
function getResponseAction(data) {
  if (data.hasOwnProperty('config:save')) {
    return () => show('config:saved');
  }
}

export default {
  [request](parameters) {
    const [idSet, promises] = normalize(parameters);

    const reduceIdValueDictionary = (accumulator, value, index) =>
      assign(accumulator, {
        [idSet[index]]: {
          $set: value,
        },
      });

    const createResponseAction = response => {
      const data = response.reduce(reduceIdValueDictionary, {});
      // ZS-FIXME: always uses a single (i.e. the first) parameters entry
      const [[part]] = parameters;
      const meta = get(part, 'meta');

      return respond(data, null, meta);
    };

    return Promise
      .all(promises)
      .then(response =>
        () => createResponseAction(response))
      .catch(reason =>
        () => abort(reason));
  },

  [respond](data) {
    const responseAction = getResponseAction(data);

    return Promise.resolve(responseAction);
  },

  [abort](reason) {
    const handler = statusHandlers[reason];
    const abortionAction = callOrNothingAtAll(handler);

    return Promise.resolve(() => abortionAction);
  },
};
