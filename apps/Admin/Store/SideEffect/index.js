import authSideEffects from './auth';
import choicesSideEffects from './choices';
import configSaveEffects from './configSave';
import routeSideEffects from './route';
import resourceSideEffects from './resource';

const list = [
  authSideEffects,
  choicesSideEffects,
  configSaveEffects,
  resourceSideEffects,
  routeSideEffects,
];

export default list.reduce((accumulator, sideEffects) => {
  Object
    .entries(sideEffects)
    .forEach(sideEffect => {
      const [key, func] = sideEffect;

      if (!accumulator[key]) {
        accumulator[key] = [];
      }

      accumulator[key].push(func);
    });

  return accumulator;
}, {});
