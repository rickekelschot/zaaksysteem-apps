# Middleware

> Middleware functions sit between invoking an action, 
  and the reducer(s) taking care of an action. 
  An action passes through all middlewares before it reaches any reducers. 
  This allows us to cancel or modify the action, or simply let it pass through.

Documentation at https://redux.js.org/advanced/middleware

## Adding middleware

1. Create and export the function in a new file in the Middleware folder
2. Import it in `index.js` and add it to the array that is being exported.  
   Actions will pass through the functions in this order.

## Example

Log whenever an action is dispatched

    const example = store =>
      next =>
        action => {
          console.log('example middleware');
          return next(action);
        };
    export default example;



