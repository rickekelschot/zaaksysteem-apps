import { createReducer } from '../../../library/redux/createReducer';
import action from '../Action';

const { assign } = Object;
const {
  set,
} = action.form;
const initialState = {};

/**
 * @type {Function}
 */
export const formReducer = createReducer(initialState, {
  [set]: ({payload, state}) => assign({}, state, payload),
});
