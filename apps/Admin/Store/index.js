import { createReduxStore } from '../../library/redux/createStore';
import { formReducer as form } from './Reducer/form';
import { logReducer as log } from './Reducer/log';
import { resourceReducer as resource } from './Reducer/resource';
import { routeReducer as route } from './Reducer/route';
import { uiReducer as ui } from './Reducer/ui';
import middlewares from './Middleware';
import sideEffects from './SideEffect';

const reducers = {
  form,
  log,
  resource,
  route,
  ui,
};

/**
 * Create a store with initial state. Since the initial state
 * can be impure, this is a factory function that is called
 * from the application.
 *
 * @param {Object} initialState
 *   The initial state of the store.
 * @return {Store}
 */
export const createStore = initialState =>
  createReduxStore(reducers, initialState, middlewares, sideEffects);
