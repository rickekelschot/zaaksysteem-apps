/*
───────────▄▄▄▄▄▄▄▄▄───────────
────────▄█████████████▄────────
█████──█████████████████──█████
▐████▌─▀███▄───────▄███▀─▐████▌
─█████▄──▀███▄───▄███▀──▄█████─
─▐██▀███▄──▀███▄███▀──▄███▀██▌─
──███▄▀███▄──▀███▀──▄███▀▄███──
──▐█▄▀█▄▀███─▄─▀─▄─███▀▄█▀▄█▌──
───███▄▀█▄██─██▄██─██▄█▀▄███───
────▀███▄▀██─█████─██▀▄███▀────
───█▄─▀█████─█████─█████▀─▄█───
───███────────███────────███───
───███▄────▄█─███─█▄────▄███───
───█████─▄███─███─███▄─█████───
───█████─████─███─████─█████───
───█████─████─███─████─█████───
───█████─████─███─████─█████───
───█████─████▄▄▄▄▄████─█████───
────▀███─█████████████─███▀────
──────▀█─███─▄▄▄▄▄─███─█▀──────
─────────▀█▌▐█████▌▐█▀─────────
────────────███████────────────
*/

import {
  isArrayOfStrings,
  isCollection,
  isDbObject,
  isSelect,
  isArrayOfSelects,
  selectToDb,
  arrayToDb,
  objectToSelect,
  arrayToSelect,
} from './library/transformers';
import {
  asArray,
  get,
  isPopulatedArray,
  reduceMap,
} from '@mintlab/kitchen-sink';

/**
 * @param {string} firstObject
 * @param {string} secondObject
 * @return {number}
 */
const sortByLabel = (firstObject, secondObject) =>
  firstObject.label.localeCompare(secondObject.label);

/**
 * @param {Object} parameters
 * @param {string} parameters.type
 * @param {boolean} parameters.required
 * @return {Array}
 */
const getConstraints = ({ type, required }) => {
  const constraints = [];

  if (['email', 'uri', 'number'].includes(type)) {
    constraints.push(type);
  }
  if (required) {
    constraints.push('required');
  }

  return constraints;
};

/**
 * @param {Object} parameters
 * @param {string} parameters.type
 * @param {string} parameters.name
 * @param {Object} parameters.config
 * @return {string}
 */
const getComponentType = ({ type, name, config }) => {
  function getFormat() {
    return get(config, 'format') === 'html' ? 'html' : 'text';
  }

  const map = new Map([
    [keyName => keyName === 'custom_relation_roles', () => 'creatable'],
    [keyName => keyName === 'allowed_templates', () => 'select'],
    [(keyName, keyType) => keyType === 'boolean', () => 'checkbox'],
    [(keyName, keyType) => keyType === 'text', () => getFormat()],
    [(keyName, keyType) => keyType === 'object_ref', () => 'select'],
  ]);

  return reduceMap({
    map,
    keyArguments: [name, type],
    valueArguments: [name, type],
    fallback: 'text',
  });
};

/**
 * @param {*} value_type
 * @return {Object|undefined}
 */
const getConfig = value_type =>
  get(value_type, 'options');

/**
 * @param {string} name
 * @return {Object|undefined}
 */
const getChoicesParameters = name => {
  const emailTemplate = {
    url: '/api/v1/email_template',
    match: 'label',
    name,
  };
  const group = {
    url: '/api/v1/group',
    match: 'description',
    name,
  };
  const role = {
    url: '/api/v1/role',
    match: 'description',
    name,
  };
  const municipalities = {
    url: '/api/v1/general/municipality_code',
    match: 'name',
    name,
    rows: 50,
  };

  const map = {
    allocation_notification_template_id: emailTemplate,
    feedback_email_template_id: emailTemplate,
    new_user_template: emailTemplate,
    subject_pip_authorization_confirmation_template_id: emailTemplate,
    case_distributor_group: group,
    case_distributor_role: role,
    signature_upload_role: role,
    bag_priority_gemeentes: municipalities,
  };

  if (map[name]) {
    return map[name];
  }
};

/**
 * @param {*} name
 * @return {boolean}
 */
const getAutoLoad = name =>
  ['case_distributor_group', 'case_distributor_role', 'signature_upload_role'].includes(name);

/**
 * Transforms field value(s) from app state values to valid backend values
 * @param {*} value
 * @return {*}
 */
const appToDb = value => {
  const map = new Map([
    [
      keyValue => isSelect(keyValue),
      valueValue => selectToDb(valueValue),
    ],
    [
      keyValue => isArrayOfSelects(keyValue),
      valueValue => valueValue.map(thisValue => selectToDb(thisValue)),
    ],
    [
      keyValue => isArrayOfStrings(keyValue),
      valueValue => arrayToDb(valueValue),
    ],
  ]);

  return reduceMap({
    map,
    keyArguments: [value],
    valueArguments: [value],
    fallback: value,
  });
};

const getChoices = value_type =>
  get(value_type, 'choices');

/**
 * Transform field value(s) from database to valid app state values
 * @param {*} value
 * @return {*}
 */
const dbToApp = value => {
  const map = new Map([
    [
      keyValue => isDbObject(keyValue),
      valueValue => objectToSelect(valueValue),
    ],
    [
      keyValue => isArrayOfStrings(keyValue),
      valueValue => arrayToSelect(valueValue),
    ],
    [
      keyValue => isCollection(keyValue),
      valueValue =>
        asArray(get(valueValue, 'instance.rows'))
          .map(row => objectToSelect(row)),
    ],
  ]);

  return reduceMap({
    map,
    keyArguments: [value],
    valueArguments: [value],
    fallback: value,
  });
};

/**
 * @type {Array}
 */
const transformers = [
  {
    match: [
      '^/api/v1/session',
    ],
    transform: response => get(response, 'result.instance'),
  },
  {
    match: [
      '^/api/v1/config/update',
    ],
    transform(response) {
      const rows = get(response, 'result.instance.rows');

      const map = ({
        reference,
        instance: {
          name,
        },
      }) => ({
        name,
        reference,
      });

      return asArray(rows).map(map);
    },
  },
  {
    match: [
      '^/api/v1/(group|role|email_template|general/municipality_code)',
    ],
    transform(response) {
      const rows = get(response, 'result.instance.rows');
      return asArray(rows)
        .map(row => objectToSelect(row))
        .sort(sortByLabel);
    },
  },
  {
    match: [
      '^/api/v1/config/panel',
    ],
    transform(response) {
      const {
        result: {
          instance: {
            definitions: { instance: { rows: definitionsRows } },
            items: { instance: { rows: itemsRows } },
          },
        },
      } = response;

      const createItemDefinition = ({
        reference: definitionReference,
        instance: {
          config_item_name: name,
          label,
          value_type,
          value_type_name,
          mvp: isMulti,
          mutable,
          required,
        },
      }) => {

        const item = itemsRows.find(({ instance: { definition: { reference: thisReference } } }) => thisReference === definitionReference);
        const reference = get(item, 'reference');
        const type = (value_type_name) ? value_type_name : (get(value_type, 'parent_type_name') || get(value_type, 'name'));
        const value = dbToApp(get(item, 'instance.value'));
        const config = getConfig(value_type);
        const constraints = getConstraints({
          type,
          required,
        });
        const componentType = getComponentType({
          type,
          name,
          config,
        });
        const choicesParameters = getChoicesParameters(name);
        const autoLoad = getAutoLoad(name);
        const choices = dbToApp(getChoices(config));
        const hasInitialChoices = isPopulatedArray(choices);
        const externalLabel = ['select', 'creatable', 'html'].includes(componentType);

        /* ZS-FIXME: */
        /* eslint complexity: [2, 8] */

        return {
          type: componentType,
          config,
          label,
          name,
          isMulti,
          disabled: !mutable,
          reference,
          required,
          hasInitialChoices,
          externalLabel,
          value,
          ...(constraints && { constraints }),
          ...(choicesParameters && { choicesParameters }),
          ...(autoLoad && { autoLoad }),
          ...(choices && { choices }),
        };
      };

      return {
        items: definitionsRows
          .map(createItemDefinition)
          .filter(value => value.reference),
      };
    },
  },
  {
    match: [
      '^/api/v1/eventlog',
    ],
    transform(response) {
      const instance = get(response, 'result.instance');
      const count = get(instance, 'pager.total_rows');
      const rows = get(instance, 'rows')
        .map(row => ({
          caseId: get(row, 'instance.case_id'),
          date: get(row, 'instance.date_created'),
          description: get(row, 'instance.event_data.human_readable'),
          component: get(row, 'instance.component'),
          user: {
            displayName: get(row, 'instance.created_by.instance.subject.instance.display_name'),
          },
        }));

      return ({
        rows,
        count,
      });
    },
  },
  {
    match: [
      '^/objectsearch/contact/medewerker',
    ],
    transform(response) {
      const entries = get(response, 'json.entries', []);
      const users = entries.map(entry => {

        const uuid = entry.object.uuid;
        const label = entry.label;

        return {
          uuid,
          value: uuid,
          label,
        };
      });

      return users;
    },
  },
  {
    match: [
      '^/api/v1/subject',
    ],
    transform(response) {
      const uuid = get(response, 'result.reference');
      const label = get(response, 'result.instance.display_name');

      return {
        uuid,
        value: uuid,
        label,
      };
    },
  },
];

export {
  transformers,
  appToDb,
};
