import resource, { expandUrlWithIdSegments, idToArray, segmentsToPath } from '.';

/**
 * @test {resource}
 */
describe('The `resource` module', () => {
  test('rejects the promise if the resource ID is unknown', () => {
    const caughtExceptionCount = 1;

    expect.assertions(caughtExceptionCount);

    resource('nil')
      .catch(reason =>
        expect(reason.message)
          .toBe('Resource: Fatal: Unknown resource ID or map "nil"'));
  });

  describe('exports an `expandUrlWithIdSegments` function that', () => {
    test('does not modify the specification URL if segments are empty', () => {
      const actual = expandUrlWithIdSegments({
        url: '/foobar',
      }, []);
      const asserted = {
        url: '/foobar',
      };

      expect(actual).toEqual(asserted);
    });

    test('appends segments as a path', () => {
      const actual = expandUrlWithIdSegments({
        url: '/foobar',
      }, ['foo', 'bar']);
      const asserted = {
        url: '/foobar/foo/bar',
      };

      expect(actual).toEqual(asserted);
    });
  });

  describe('has an `idToArray` function that', () => {
    test('returns an array with only the argument if it does not contain whitespace', () => {
      const actual = idToArray('foo');
      const asserted = ['foo'];

      expect(actual).toEqual(asserted);
    });

    test('splits whitespace separated tokens into an array', () => {
      const actual = idToArray('foo bar');
      const asserted = ['foo', 'bar'];

      expect(actual).toEqual(asserted);
    });
  });

  describe('has an `segmentsToPath` function that', () => {
    test('joins array elements to a path', () => {
      const actual = segmentsToPath(['foo', 'bar']);
      const asserted = 'foo/bar';

      expect(actual).toBe(asserted);
    });
  });
});
