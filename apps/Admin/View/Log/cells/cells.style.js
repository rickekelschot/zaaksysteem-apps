const separatorMargin = '8px';
const separatorSize = '4px';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const cellStyleSheet = ({
  mintlab: {
    greyscale,
  },
  palette: {
    primary,
  },
}) => ({
  link: {
    color: primary.main,
    fontWeight: 'bold',
    textDecoration: 'none',
  },
  dateTime: {
    overflow: 'hidden',
  },
  dateTimeWrapper: {
    whiteSpace: 'nowrap',
    display: 'inline-block',
  },
  date: {
    marginRight: `calc(${separatorMargin} * 2 + ${separatorSize})`,
  },
  time: {
    color: greyscale.evenDarker,
    marginLeft: `calc(0px - ${separatorMargin} - ${separatorSize})`,
    '&::before': {
      content: '"·"',
      fontWeight: 'bold',
      marginRight: separatorMargin,
    },
  },
  component: {
    color: greyscale.evenDarker,
  },
  displayName: {
    whiteSpace: 'nowrap',
    margin: '8px',
  },
});
