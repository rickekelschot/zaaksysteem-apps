import {
  createExternalParams,
  getPageByRows,
  getRows,
  getCount,
  getUserOptions,
  getInitialUser,
  numericUiToNumbers,
} from './LogContainer';
import formatRows from './library/formatRows';

describe('The `LogContainer` module', () => {
  describe('The `numericUiToNumbers` function that', () => {
    test('`returns an array of numbers from a string of spaced numbers`', () => {
      const actual = numericUiToNumbers('-10 0 1 10 100');
      const expected = [-10, 0, 1, 10, 100]; // eslint-disable-line no-magic-numbers

      expect(actual).toEqual(expected);
    });
  });

  describe('The `getRows` function that', () => {
    const rows = [{
      caseId: 42,
      component: 'case',
      date: '2018-12-19T08:48:44Z',
      description: 'Hello, world',
      user: {
        displayName: 'Fred Foobar',
      },
    },
    {
      caseId: 69,
      component: 'subject',
      date: '2018-12-19T08:48:44Z',
      description: 'Goodbye, cruel world',
      user: {
        displayName: 'Fred Fourchan',
      },
    }];

    test('`returns the formatted rows from the resource`', () => {
      const actual = getRows({
        data: {
          rows,
        },
      });
      const expected = formatRows(rows);

      expect(actual).toEqual(expected);
    });

    test('`returns null if the resource does not contain rows`', () => {
      const actual = getRows({
        geordi: 'La Forge',
      });
      const expected = null;

      expect(actual).toEqual(expected);
    });
  });

  describe('The `getCount` function that', () => {
    test('`returns the count from the resource`', () => {
      const actual = getCount({
        data: {
          count: 42,
        },
      });
      const expected = 42;

      expect(actual).toEqual(expected);
    });

    test('`returns the initial count if the resource does not contain one`', () => {
      const actual = getCount({
        geordi: 'Lag Forge',
      });
      const expected = 0;

      expect(actual).toEqual(expected);
    });
  });

  describe('The `getUserOptions` function that', () => {
    test('`returns the userOptions from the resource`', () => {
      const data = ['user', 'options'];
      const actual = getUserOptions({data});
      const expected = data;

      expect(actual).toEqual(expected);
    });

    test('`returns an empty array if the resource does not contain userOptions`', () => {
      const actual = getUserOptions({geordi: 'La Forge'});
      const expected = [];

      expect(actual).toEqual(expected);
    });
  });

  describe('The `getInitialUser` function that', () => {
    test('`returns the initialUser from the resource`', () => {
      const data = 'Fred Foobar';
      const actual = getInitialUser({data});
      const expected = data;

      expect(actual).toEqual(expected);
    });
  });

  describe('The `createExternalParams` function that', () => {
    test('`returns an object with externalKeys instead of internalKeys`', () => {
      const keyword = 'foo';
      const caseNumber = '42';
      const user = {
        id: 42,
        label: 'Fred Foobar',
      };
      const data = { keyword, caseNumber, user };
      const actual = createExternalParams(data);
      const expected = {
        'query:match:keyword': keyword,
        'query:match:case_id': caseNumber,
        'query:match:subject': user,
      };

      expect(actual).toEqual(expected);
    });
  });

  describe('The `getPageByRows` function returns the', () => {
    test('`number of the next page when decreasing the rows`', () => {
      const page = 2;
      const rowsPerPage = 50;
      const nextRowsPerPage = 25;

      const actual = getPageByRows(page, rowsPerPage, nextRowsPerPage);
      const expected = 4;

      expect(actual).toBe(expected);
    });

    test('`number of the next page when increasing the rows`', () => {
      const page = 4;
      const rowsPerPage = 25;
      const nextRowsPerPage = 50;

      const actual = getPageByRows(page, rowsPerPage, nextRowsPerPage);
      const expected = 2;

      expect(actual).toBe(expected);
    });

    test('`number of the next page when increasing the rows and the watched result isnt the first result of the next sort`', () => {
      const page = 2;
      const rowsPerPage = 25;
      const nextRowsPerPage = 100;

      const actual = getPageByRows(page, rowsPerPage, nextRowsPerPage);
      const expected = 0;

      expect(actual).toBe(expected);
    });
  });
});
