/**
 * @return {JSS}
 */
export const exportButtonStyleSheet = () => ({
  wrapper: {
    whiteSpace: 'nowrap',
  },
});
