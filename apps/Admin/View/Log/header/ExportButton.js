import React from 'react';
import classNames from 'classnames';
import { exportButtonStyleSheet } from './ExportButton.style';
import {
  withStyles,
  Button,
} from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.baseClassName
 * @param {Function} props.action
 * @param {Object} props.exportParams
 * @param {string} props.value
 * @return {ReactElement}
 */
export const ExportButton = ({
  classes,
  baseClassName,
  action,
  exportParams,
  value,
}) => (
  <div className={classNames(baseClassName, classes.wrapper)}>
    <Button
      action={() => action(exportParams)}
      presets={['secondary', 'contained']}
    >
      {value}
    </Button>
  </div>
);

export default withStyles(exportButtonStyleSheet)(ExportButton);
