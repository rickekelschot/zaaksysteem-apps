/*
 * Development configuration for base segment routing, extends stable.
 */

import stable from './base.iframe';

const { assign } = Object;

const development = {};

export default assign(stable, development);
