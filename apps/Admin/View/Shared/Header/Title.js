import React from 'react';
import classNames from 'classnames';
import { titleStylesheet } from './Title.style.js';
import { withStyles, H3 } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Title = ({
  titleStyle,
  classes,
  children,
}) => (
  <H3
    classes={{
      root: classNames(classes.title, titleStyle),
    }}
  >
    {children}
  </H3>
);

export default withStyles(titleStylesheet)(Title);
