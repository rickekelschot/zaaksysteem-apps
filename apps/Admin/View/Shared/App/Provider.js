/*
 * App entry component.
 */
import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { MaterialUiThemeProvider } from '@mintlab/ui';
import ErrorBoundary from './ErrorBoundary';
import Locale from './Locale';
import Resource from '../ResourceContainer';
import { createStore } from '../../../Store';
import action from '../../../Store/Action';
import { onPopState } from '../../../../library/dom/history';
import { getUrl } from '../../../../library/url';

const {
  route: {
    resolve,
  },
} = action;
const initialState = {
  route: getUrl(),
};
const store = createStore(initialState);

function dispatchRoute() {
  store.dispatch(resolve({
    path: getUrl(),
  }));
}

onPopState(dispatchRoute);

const resources = [
  'locale',
  'navigation',
  'session',
];

const Provider = () => (
  <StoreProvider store={store}>
    <MaterialUiThemeProvider>
      <ErrorBoundary>
        <Resource id={resources}>
          <Locale/>
        </Resource>
      </ErrorBoundary>
    </MaterialUiThemeProvider>
  </StoreProvider>
);

export default Provider;
