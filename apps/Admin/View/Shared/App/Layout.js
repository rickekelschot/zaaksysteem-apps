import { createElement } from 'react';
import { Layout, withStyles } from '@mintlab/ui';
import {
  extract,
  getSegment,
} from '@mintlab/kitchen-sink';
import ErrorBoundary from './ErrorBoundary';
import View from './View';
import { logout } from '../../../../library/auth';
import { navigate } from '../../../../library/url';
import routes from '../../library/router/base';
import { layoutStylesheet } from './Layout.style';

const INTERN_URL = '/intern/';
const ABOUT_URL = '/intern/!over';
const SUPPORT_URL = 'https://help.zaaksysteem.nl/';

/**
 * @param {Array} data
 * @param {string} url
 * @return {number}
 */
const getNavigationIndex = (data, url) => {
  const segment = getSegment(url);
  const index = data
    .map(item => getSegment(item.path))
    .indexOf(segment);

  return index;
};

/**
 * @param {Array} input
 * @param {Function} route
 * @return {Array}
 */
const mapToDrawer = (input, route) =>
  input
    .map(({ icon, label, path }) => ({
      action() {
        route({
          path,
        });
      },
      icon,
      label,
    }));

/**
 * @param {string} requestUrl
 * @param {Object} classes
 * @return {Object}
 */
const getContentClasses = (requestUrl, classes) => {
  const segment = getSegment(requestUrl);
  const content = (typeof routes[segment] === 'string')
    ? classes.legacy
    : classes.next;

  return {
    content,
  };
};

/**
 * Wrap the {@link View} in the `@mintlab/ui` Layout.
 *
 * @param {Object} props
 * @param {Array} props.banners
 * @param {Object} props.classes
 * @param {string} props.company
 * @param {Array} props.navigation
 * @param {Function} props.t
 * @param {Function} props.toggleDrawer
 * @param {string} props.user
 * @return {ReactElement}
 */
export const AdminLayout = props => {
  const { t, classes } = props;
  const [
    company,
    userName,
    navigation,
    isDrawerOpen,
    toggleDrawer,
    banners,
    iframeProps,
  ] = extract('company', 'userName', 'userNavigation', 'isDrawerOpen', 'toggleDrawer', 'banners', props);
  const {
    requestUrl,
    route,
  } = iframeProps;

  const drawer = {
    primary: mapToDrawer(navigation, route),
    secondary: [
      {
        href: INTERN_URL,
        icon: 'sync',
        label: t('navigation:handle'),
      },
      {
        href: SUPPORT_URL,
        target: '_blank',
        icon: 'help',
        label: t('navigation:support'),
      },
    ],
    about: {
      action: () => navigate(ABOUT_URL),
      label: t('navigation:about'),
    },
  };

  const userActions = [
    {
      action: logout,
      icon: 'power_settings_new',
      label: t('navigation:logout'),
    },
  ];

  const CurrentView = createElement(ErrorBoundary, null, createElement(View, iframeProps));

  return createElement(Layout, {
    active: getNavigationIndex(navigation, requestUrl),
    banners,
    classes: getContentClasses(requestUrl, classes),
    company,
    drawer,
    identity: `${t('common:title')} ${t('common:admin')}`,
    isDrawerOpen,
    menuLabel: t('aria:mainMenu'),
    toggleDrawer,
    userActions,
    userLabel: t('aria:userMenu'),
    userName,
  }, CurrentView);
};

export default withStyles(layoutStylesheet)(AdminLayout);
