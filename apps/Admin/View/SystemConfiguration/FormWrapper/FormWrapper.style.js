import config from './config.svg';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formWrapperStylesheet = ({
  breakpoints,
}) => ({
  sheet: {
    flex: 1,
    padding: '30px',
    'background-image': `url(${config})`,
    'background-size': '18px',
  },
  form: {
    [breakpoints.up('lg')]: {
      maxWidth: breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
});
