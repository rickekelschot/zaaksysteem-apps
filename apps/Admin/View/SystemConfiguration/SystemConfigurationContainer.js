import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { fieldsChanged } from '../../../library/form';
import SystemConfiguration from './SystemConfiguration';
import Actions from '../../Store/Action';
import { getUrl } from '../../../library/url';
import {
  get,
  getSegment,
} from '@mintlab/kitchen-sink';

const { assign } = Object;
const DEFAULT_ROWS = 200;
const TEXTFIELD_ROWS = 10;

/**
 * @type {Object}
 */
const categoryIcons = {
  cases: 'folder_shared',
  users: 'supervised_user_circle',
  pip: 'web',
  documents: 'insert_drive_file',
  about: 'fingerprint',
  premium: 'star',
  other: 'card_giftcard',
  deprecated: 'save',
};

/**
 * @param {Object} state
 * @param state.route
 * @param state.form
 * @param state.resource
 * @param state.ui
 * @return {Object}
 */
const mapStateToProps = ({ route, form, resource, ui }) => ({
  route,
  form,
  resource,
  banners: ui.banners,
});

/**
 * @param {Function} dispatch
 * @return {Object}
 */
const mapDispatchToProps = dispatch => ({
  invoke: bindActionCreators(Actions.route.invoke, dispatch),
  set: bindActionCreators(Actions.form.set, dispatch),
  request: bindActionCreators(Actions.resource.request, dispatch),
  showBanner: bindActionCreators(Actions.ui.banner.show, dispatch),
  hideBanner: bindActionCreators(Actions.ui.banner.hide, dispatch),
  showDialog: bindActionCreators(Actions.ui.dialog.show, dispatch),
});

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    resource: {
      config: {
        data: {
          items: resourceItems,
        },
      },
      systemConfiguration: {
        data: formDefinition,
      },
    },
    form,
  } = stateProps;

  const { request } = dispatchProps;
  const createCustomChoicesRequest = ({
    url,
    name,
    input,
    match,
    rows = DEFAULT_ROWS,
  }) =>
    request([
      {
        id: 'choices',
        method: 'GET',
        url,
        meta: {
          name,
        },
      },
      {
        [`query:match:${match}`]: input,
        rows,
      },
    ]);

  /**
   * Replace the field name from the configuration file with the actual definition.
   *
   * @param {Array} accumulator
   * @param {string} name
   * @return {Array}
   */
  const reduceField = (accumulator, name) => {
    const item = resourceItems.find(resourceItem => resourceItem.name === name);

    if (item) {
      const choicesParameters = get(item, 'choicesParameters');
      const mergedItem = assign({}, item, {
        get value() {
          if (form.hasOwnProperty(name)) {
            return form[name];
          }

          return get(item, 'value');
        },
        get getChoices() {
          if (choicesParameters) {
            return input =>
              createCustomChoicesRequest(assign({}, choicesParameters, { input }));
          }
        },
        get translations() {
          if (['select', 'creatable'].includes(item.type)) {
            return ['loading', 'choose', 'beginTyping', 'creatable', 'create']
              .reduce((translationAccumulator, entry) => {
                translationAccumulator[`form:${entry}`] = t(`form:${entry}`);
                return translationAccumulator;
              }, {});
          }
        },
        get rows() {
          return TEXTFIELD_ROWS;
        },
        get help() {
          return t(`systemConfiguration:help:${name}`);
        },
      });

      accumulator.push(mergedItem);
    }

    return accumulator;
  };

  const { segments, t } = ownProps;
  const [routeSegment] = segments;

  /**
   * @param {string} title
   * @param {string} description
   * @param {Object} fields
   * @return {Object}
   */
  const mapFieldSet = ({
    title,
    description,
    fields,
  }) => ({
    ...(title && { title: t(`systemConfiguration:titles:${title}`) }),
    ...(description && { description: t(`systemConfiguration:descriptions:${description}`) }),
    fields: fields.reduce(reduceField, []),
  });

  /**
   * @type Array
   */
  const fullMap = formDefinition
    .map(({
      slug,
      fieldSets,
    }) => ({
      current: (routeSegment === slug),
      fieldSets: fieldSets.map(mapFieldSet),
      slug,
    }));

  /**
   * @type Array
   */
  const categories = fullMap
    .map(({
      slug,
      current,
    }) => ({
      label: t(`systemConfiguration:categories:${slug}`),
      icon: categoryIcons[slug],
      slug,
      current,
    }));

  const [currentItem] = fullMap.filter(({ current }) => current);
  const identifier = get(currentItem, 'slug');
  const fieldSets = get(currentItem, 'fieldSets');
  const changed = fieldsChanged(form, resourceItems);

  if (!currentItem) {
    const [{
      slug: defaultSlug,
    }] = fullMap;
    const segment = getSegment(getUrl());

    ownProps.route({
      force: true,
      path: `/admin/${segment}/${defaultSlug}`,
      replace: true,
    });
  }

  return assign({}, stateProps, dispatchProps, ownProps, {
    categories,
    fieldSets,
    identifier,
    changed,
  });
};

/**
 * Connects {@link SystemConfiguration} with {@link i18next} and the store.
 *
 * @return {ReactElement}
 */
const SystemConfigurationContainer = translate()(connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SystemConfiguration));

export default SystemConfigurationContainer;
