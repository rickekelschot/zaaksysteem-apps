const ERROR = 'error';
const ID_LENGTH = 2;

module.exports = {
  parser: 'babel-eslint',
  extends: [
    '@mintlab/react',
  ],
  rules: {
    'id-length': [
      ERROR, {
        min: ID_LENGTH,
        exceptions: [
          't',
        ],
      },
    ],
  },
};
