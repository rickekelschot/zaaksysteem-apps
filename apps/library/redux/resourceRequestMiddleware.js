import { normalize } from './resource';

const { assign } = Object;
// ZS-TODO: meh (factory?)
const RESOURCE_REQUEST_ACTION_TYPE = 'RESOURCE:REQUEST';

export const resourceRequestMiddleware = () =>
  next =>
    action => {
      const { type } = action;

      if (type === RESOURCE_REQUEST_ACTION_TYPE) {
        const { payload } = action;
        const normalizedResourceRequestAction = assign({}, action, {
          payload: normalize(payload),
        });

        return next(normalizedResourceRequestAction);
      }

      return next(action);
    };
