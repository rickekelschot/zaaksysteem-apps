import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { reduxSideEffectsMiddlewareFactory } from './sideEffectsMiddlewareFactory';
import { resourceRequestMiddleware } from './resourceRequestMiddleware';

/**
 * @param {Object} reducers
 * @param {Object} preloadedState
 * @param {Array} middlewares
 * @param {Object} sideEffects
 * @return {Store<any>}
 */

export function createReduxStore(reducers, preloadedState, middlewares, sideEffects) {
  const { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ } = window;
  const composeEnhancers = (__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose);

  const middleware = [
    resourceRequestMiddleware,
    ...middlewares,
    reduxSideEffectsMiddlewareFactory(sideEffects),
  ];

  const enhancer = composeEnhancers(applyMiddleware(...middleware));

  return createStore(combineReducers(reducers), preloadedState, enhancer);
}
