const exec = require('./exec');
const DELETE = 0;
const OFFSET = 1;

/**
 * @param {Array} list
 * @param {string} context
 * @return {Array}
 */
function getContextArguments(list, context) {
  if (context === 'build') {
    return list;
  }

  const prefix = ['--prefix', `./${context}`];

  list.splice(OFFSET, DELETE, ...prefix);

  return list;
}

/**
 * @param {string} context
 * @param {Array} argumentList
 * @return {Promise}
 */
const npmFactory = (context, argumentList) =>
  exec('npm', getContextArguments(argumentList.filter(value => value), context));

/**
 * @param {string} context
 * @param {string} packageName
 * @return {Promise}
 */
const install = (context, packageName) =>
  npmFactory(context, [
    'install',
    '--no-optional',
    '--loglevel',
    'error',
    packageName,
  ]);

/**
 * @param {string} context
 * @param {string} packageName
 * @return {Promise}
 */
const uninstall = (context, packageName) =>
  npmFactory(context, [
    'uninstall',
    '--loglevel error',
    packageName,
  ]);

module.exports = {
  install,
  uninstall,
};
