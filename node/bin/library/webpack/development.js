// Enable swapping of sub-apps under /admin/* with the environment variable
// WEBPACK_BUILD_TARGET=development

const { NormalModuleReplacementPlugin } = require('webpack');

const replacements = [
  new NormalModuleReplacementPlugin(
    /\/library\/router\/base.js$/,
    'base.development.js'
  ),
];

/**
 * @param {Object} configuration
 * @param {Array} configuration.plugins
 */
function development({ plugins }) {
  plugins.push(...replacements);
}

module.exports = development;
