const { readFileSync, writeFileSync } = require('fs');
const { extname } = require('path');

const { parse, stringify } = JSON;
const ENCODING = 'utf8';
const JSON_EXTENSION = '.json';
const INDENT = 2;

/**
 * @param {string} path
 * @return {boolean}
 */
const isJson = path =>
  extname(path) === JSON_EXTENSION;

/**
 * @param {Object} data
 * @return {string}
 */
const toJsonString = data =>
  stringify(data, null, INDENT);

/**
 * @param {string} path
 * @return {*}
 */
function read(path) {
  const literal = readFileSync(path, ENCODING);

  if (isJson(path)) {
    return parse(literal);
  }

  return literal;
}

function write(path, data) {
  const content = isJson(path) ?
    toJsonString(data) :
    data;

  writeFileSync(path, content, ENCODING);
}

module.exports = {
  read,
  write,
};
