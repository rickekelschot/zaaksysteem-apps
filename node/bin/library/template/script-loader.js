/* global document, __SCRIPT_QUEUE__, __STYLE_SHEET_QUEUE__ */
/* eslint-disable complexity, func-names, no-var, strict */
/* eslint no-empty: ["error",  { allowEmptyCatch: true }] */

/**
 * ECMAScript 5 script and style sheet loader.
 * Assets are asynchronously loaded in parallel,
 * scripts are executed in order.
 *
 * `(__VARIABLE__)` tokens are replaced with template values.
 * Those should be valid ECMAScript identifiers for easy editing
 * of this file. Surrounding them with expression parentheses
 * makes them unique enough for naive string replacement of a
 * single occurance.
 *
 * For anything more advanced, use a parser that produces a mutable AST.
 */
(function () {
  'use strict';
  var hasBlockScope;

  try {
    // eslint-disable-next-line no-eval
    hasBlockScope = eval('{const x=1}typeof x=="undefined"');
  } catch (ignore) {
  }

  // IE 11 and better
  if (hasBlockScope) {
    var domDocument = document;
    var scriptQueue = (__SCRIPT_QUEUE__);

    // IE 11
    if (Array.prototype.includes === undefined) {
      scriptQueue.unshift('(__POLYFILL__)');
    }

    var SCRIPT_ELEMENT = 'SCRIPT';
    var headElement = domDocument.head;

    var onError = function() {
      if (this.nodeName.toLowerCase() === 'script') {
        throw 'could not load script: ' + this.src;
      }

      throw 'could not load style sheet: ' + this.href;
    };

    scriptQueue
      .forEach(function (src) {
        var script = domDocument.createElement(SCRIPT_ELEMENT);

        script.onerror = onError;
        script.src = src;
        script.async = false;
        headElement.appendChild(script);
      });

    (__STYLE_SHEET_QUEUE__)
      .forEach(function (href) {
        var link = domDocument.createElement('LINK');

        link.onerror = onError;
        link.href = href;
        link.rel = 'stylesheet';
        headElement.appendChild(link);
      });
  }
}());
